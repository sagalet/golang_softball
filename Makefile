
GOROOT := $(shell go env GOROOT)

ifeq ($(CROSS_COMPILE), true) 
GO := $(GOROOT)/bin/gox
else
GO := $(GOROOT)/bin/go
endif

PROTOC := protoc

GOPATH := $(shell pwd)
OUT := out
GO_DEFAULT_PATH := $(shell go env GOPATH):$(GOPATH)

PACKAGES_CONFIG_SRC := \
	src/packages/config/reader.go \
	src/packages/config/data.go

all: packages web

PROTOC_PACKAGES := content1
PROTOC_SRC := ./proto/message.proto

packages: packages/config $(addprefix out/proto/src/, $(PROTOC_PACKAGES))

out/proto/src/content1: $(PROTOC_SRC)
	/bin/mkdir -p $@
	$(PROTOC) -I=./proto --go_out=$@ $^

packages/config: $(PACKAGES_CONFIG_SRC)
	GOPATH=$(GO_DEFAULT_PATH) $(GO) install $@

web:
	GOPATH=$(GO_DEFAULT_PATH):$(GOPATH)/out/proto GOBIN=$(GOPATH)/out/bin $(GO) $(CROSS_PARAMETER) install -v $@

clean:
	rm -rf out

.PHONY: all clean
