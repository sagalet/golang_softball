package main

import (
	"content1"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/golang/protobuf/proto"
	"html/template"
	"log"
	"net/http"
	"os"
	"packages/config"
	"path/filepath"
	"time"
)

var RES_PATH = "../../res"
var HTTP_PATH = "../../html"
var JS_PATH = "../../js"
var RUNTIME_PATH string

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		t := template.New("index.html")
		path := fmt.Sprintf("%s/%s/index.html", RUNTIME_PATH, HTTP_PATH)
		log.Printf("%s", path)
		t, _ = t.ParseFiles(path)
		t.Execute(w, nil)
	})
	http.HandleFunc("/pic/", func(w http.ResponseWriter, r *http.Request) {
		path := fmt.Sprintf("%s/%s/%s", RUNTIME_PATH, RES_PATH, r.URL.Path[0:])
		log.Printf("%s", path)
		http.ServeFile(w, r, path)
	})

	http.HandleFunc("/js/", func(w http.ResponseWriter, r *http.Request) {
		path := fmt.Sprintf("%s/%s/%s", RUNTIME_PATH, JS_PATH, r.URL.Path[4:])
		log.Printf("%s", path)
		w.Header().Set("Content-Type", "application/javascript; charset=utf-8")
		http.ServeFile(w, r, path)
	})

	http.HandleFunc("/json/", func(w http.ResponseWriter, r *http.Request) {
		c := config.GetConfig("config/default.json")
		b := config.GetByteArray(c)
		log.Println("send json object")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write(b)
	})

	http.HandleFunc("/video/", func(w http.ResponseWriter, r *http.Request) {
		file, err := os.Open("test.mp4")
		if err != nil {
			log.Println("open file failed err=", err)
		}
		defer file.Close()

		w.Header().Add("Content-Type", "video/mp4")

		log.Println("now start write ", time.Now())
		http.ServeContent(w, r, "test.mp4", time.Now(), file)
		log.Println("now end write ", time.Now())
	})

	http.HandleFunc("/proto/", func(w http.ResponseWriter, r *http.Request) {
		d := content1.Pack1{
			Data: []*content1.Data1{
				{
					Name: "user1",
					Id:   1,
					Msg:  "just user1"},
				{
					Name: "user2",
					Id:   2,
					Msg:  "this is user2"},
			},
		}

		out, err := proto.Marshal(&d)
		if err != nil {
			log.Fatalln("failed to marshal: ", err)
		}
		log.Printf("data=%s", out[0:])
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write(out)
	})

	http.HandleFunc("/redis/", func(w http.ResponseWriter, r *http.Request) {
		client := redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "", // no password set
			DB:       0,  // use default DB
		})
		val, err := client.Get("name").Result()
		if err != nil {
			panic(err)
		}
		log.Println("name=", val)
	})

	ex, err := os.Executable()
	RUNTIME_PATH, err = filepath.Abs(filepath.Dir(ex))
	if err != nil {
		log.Printf("failed to get path")
	}
	log.Printf("start server in %s!!", RUNTIME_PATH)

	log.Fatal(http.ListenAndServe(":11010", nil))
}
