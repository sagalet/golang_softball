FROM ubuntu

# update & upgrade
RUN apt-get update
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get upgrade

# install request package
RUN apt-get install -y git wget tar make
RUN wget https://dl.google.com/go/go1.10.3.linux-amd64.tar.gz
RUN tar -zxf go1.10.3.linux-amd64.tar.gz

# export go path
ENV PATH=$PATH:/go/bin:/root/go/bin
ENV GOROOT=/go/

# install protobuf
RUN wget https://github.com/google/protobuf/releases/download/v3.6.1/protobuf-all-3.6.1.tar.gz
RUN tar -zxf protobuf-all-3.6.1.tar.gz
WORKDIR protobuf-3.6.1
RUN apt-get install -y gcc g++ file
RUN ./configure --prefix=/usr
RUN make && make install
WORKDIR /

# get request go packages
RUN go get -u github.com/golang/protobuf/protoc-gen-go
RUN go get -u github.com/go-redis/redis

# get source code and make
RUN git clone https://gitlab.com/sagalet/golang_softball.git
WORKDIR golang_softball
RUN make all
WORKDIR /

# export port
EXPOSE 11010/tcp
EXPOSE 11010/udp

# enable service when starting
ENTRYPOINT ["/golang_softball/out/bin/web"]
